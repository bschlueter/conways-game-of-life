#!/usr/bin/env python
import time
from typing import Any, List, Tuple


SQUARE = ((1, 1), (1, 2),
          (2, 1), (2, 2))

BEACON = ((1, 1), (1, 2),
          (2, 1),
                                 (3, 4),
                         (4, 3), (4, 4))

BLINKER = ((3, 2), (3, 3), (3, 4))

GLIDER = (        (0, 1),
                          (1, 2),
          (2, 0), (2, 1), (2, 2))

Grid = List[List]
Coords = Tuple[int, int]


def copy_nested_lists(nested_list: Grid) -> Grid:
    copy = []
    for sub_list in nested_list:
        copy.append(sub_list.copy())
    return copy


def homogenous_grid(size: Coords, value: Any = False) -> Grid:
    return [[value for _ in range(size[0])] for _ in range(size[1])]


class CGOL:
    dead = "\u001b[40;1m  "
    alive = "\u001b[41;1m  "
    reset = "\u001b[0m"
    cursor_up = "\u001b[{}A"
    cursor_left = "\u001b[{}D"

    def __init__(self, size: Coords = (16, 16), positions: List[Coords] = []) -> None:
        self.size = size
        self.false_grid = homogenous_grid(self.size, False)
        initial_state = copy_nested_lists(self.false_grid)
        for pos in positions:
            initial_state[pos[0]][pos[1]] = True
        self.iterations = [self.false_grid, initial_state]
        self.print_grid(initial_state)

    def __not_repeat(self) -> None:
        ultimate_iteration = self.iterations[-1]
        penultimate_iteration = self.iterations[-2]
        for y in range(self.size[0]):
            for x in range(self.size[1]):
                if not ultimate_iteration[y][x] == penultimate_iteration[y][x]:
                    return True
        return False

    def start(self) -> None:
        try:
            while self.__not_repeat():
                time.sleep(0.5)
                self.step()
        except KeyboardInterrupt:
            pass

    def step(self) -> None:
        neighbor_counts = self.get_neighbor_counts(self.get_iteration())
        next_state = self.calculate_transition(neighbor_counts)
        self.iterations.append(next_state.copy())
        self.reset_cursor()
        self.print_grid()

    def get_iteration(self, iteration: int = -1) -> None:
        return self.iterations[iteration]

    def print_grid(self, state: list = []) -> None:
        if not state:
            state = self.get_iteration()
        for line in state:
            print("".join(map(lambda x: self.alive if x else self.dead, line)) + self.reset)

    def reset_cursor(self):
        print(self.cursor_up.format(self.size[0] + 1) + self.cursor_left.format(self.size[1]))

    def get_neighbor_counts(self, state: Grid) -> Grid:
        state = self.get_iteration()
        neighbor_counts = [[0 for _ in range(len(state))] for _ in range(len(state[0]))]
        for y in range(self.size[0]):
            for x in range(self.size[1]):
                if state[y][x]:
                    # T
                    for loc in ((y + 1, x - 1), (y + 1, x), (y + 1, x + 1),
                                (y, x - 1),                     (y, x + 1),
                                (y - 1, x - 1), (y - 1, x), (y - 1, x + 1)):
                        try:
                            current_count = neighbor_counts[loc[0]][loc[1]]
                            neighbor_counts[loc[0]][loc[1]] = current_count + 1
                        except IndexError:
                            pass
        return neighbor_counts

    def calculate_transition(self, neighbor_counts: Grid) -> Grid:
        def is_alive(neighbor_count, previous_state):
            return (previous_state and neighbor_count == 2) or neighbor_count == 3
        new_state = copy_nested_lists(self.false_grid)
        for y in range(self.size[0]):
            for x in range(self.size[1]):
                new_state[y][x] = is_alive(neighbor_counts[y][x], self.iterations[-1][y][x])
        return new_state


if __name__ == "__main__":
    game = CGOL(positions=GLIDER)
    game.start()
